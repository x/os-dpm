============
Installation
============

At the command line::

    $ pip install os-dpm

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv os-dpm
    $ pip install os-dpm
