===============================
os-dpm
===============================

OpenStack Shared library for IBM z Systems PR/SM hypervisor in DPM mode

The IBM z Systems (and IBM LinuxONE) "Dynamic Partition Manager" (DPM) allows
managing the firmware-based logical partition hypervisor (PR/SM) with the
dynamic capabilities known from software-based hypervisors. This project
holds common files between nova-dpm [1] and networking-dpm [2].

[1] https://github.com/openstack/nova-dpm
[2] https://github.com/openstack/networking-dpm


* Free software: Apache license
* Documentation: TBD
* Source: http://git.openstack.org/cgit/openstack/os-dpm
* Bugs: http://bugs.launchpad.net/os-dpm

Features
--------

* TODO
